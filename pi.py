#!/usr/bin/python3
# Name........: pi.py
# Date........: 03-13-2024
# By..........: John Mark Mobley
# Occasion....: This is in celebration of Pi Day 2024
# Description.: Use the Pythagorean Theorem to calculate pi.
# Purpose 1...: Calculate the value of pi
# Purpose 2 ..: To give knowledge, understanding, and wisdom.
# Purpose 3 ..: To aid in education.
# Purpose 4 ..: To teach programming.
# License.....: MIT License: https://github.com/aws/mit-0
# Copyright...: Copyright <2024> <John Mark Mobley>
# Environment.: Linux: Raspberry Pi OS
# Linux Shell.: Bourne Again SHell (bash)
# Language....: Python3
# Hardware....: Raspberry Pi 4 Model B with 8 GB of RAM
# Executable..: chmod +x pi.py
# How to Run 1: python3 pi.py
# How to Run 2: ./pi.py
# How to Run 3: ./pi.py -h
# How to Run 4: ./pi.py --help
# How to Run 5: ./pi.py --test
# How to Run 6: ./pi.py --gibberish 1 2 3
# How to Run 7: time ./pi.py --test
# Results.....: Version 2: 3.1415926535804317
# ............: Version 1: 3.141592653589793
# ............: Version 0: 3.141592653589793
# ............:
# ............: real 0m18.737s
# ............: user 0m18.658s
# ............: sys  0m0.029s
# How to Run 8: import pi
# ............: print(pi.pi())
# ............: print(pi.pi(number_of_segments=100, version=2))
# Speed.......: This single-threaded program takes about 19
# ............: seconds to run and gives 11 digits of pi.
# Extra Credit: Make the code work on a Linux cluster.
# ............: And read the code for more ideas.

import math
import sys

def find_the_distance_between_two_points(
        x1, y1, x2, y2, version = 0):
    """
Find the distance between two points
  version 0: Use the straight line approach.
  version 1: Use a curved line approach.
  .........: Measure distance with a radical
  .........: experimental approach. That is not
  .........: yet implemented.
"""
    if version == 0:
        # Use the straight line approach
        delta_x = abs(x1 - x2)
        delta_y = abs(y1 - y2)
        # Let's use the Pythagorean Theorem
        distance = math.sqrt(delta_x**2 + delta_y**2)

    elif version == 1:
        # Use a curved line approach. Measure distance with
        # a radical experimental approach. That is not yet
        # implemented.
        # Note 1: This is stub code for now.
        # Note 2: You may be able to use a crude value of pi
        # ......: to find a better value of pi. And then use
        # ......: recursion to improve the value some more.
        # ......: Perhaps we could start with crude_pi = 3.0
        distance = 0.0

    else:
        print(
"Error in function find_the_distance_between_two_points()\n"
"Version error")
        # Create an error just for fun.
        distance = 1.0 / 0.0
        pass # no operation (nop)

    return(distance)

def pi(number_of_segments = 10000000, version = 2):
    """
Calculate the value of pi.
  Use the number_of_segments to set the precision of the
  calculation.
  Ex 1: number_of_segments =   100
  Ex 2: number_of_segments =  1000
  Ex 3: number_of_segments = 10000
  version 0: Just return the value of pi for comparison.
  version 1: Use the arctangent approach.
  version 2: Use the Pythagorean Theorem approach.
  version 3: Use the Pythagorean Theorem approach
  .........: and measure distance with a radical
  .........: experimental approach. That is not
  .........: yet implemented.
"""    
    if version == 0:
        # version 0: Just return the value of pi for
        # .........: comparison.
        pi = math.pi

    elif version == 1:
        # version 1: Use the arctangent approach.

        # Take the arctangent of a 45.0 degree
        # right triangle and multiply by 4.0 and
        # this should give us 180.0 degrees or pi
        # Note 1: If the calculator is in degrees mode
        # ......: then the calculator will return
        # ......: 180.0 degrees
        # Note 2: if a calculator is in radians mode
        # ......: then the calculator will give the
        # ......: value of pi
        pi = math.atan(1.0) * 4.0

    elif (version == 2) or (version == 3):
        # version 2: Use the Pythagorean Theorem approach.
        # version 3: Use the Pythagorean Theorem approach
        # .........: and measure distance with a radical
        # .........: experimental approach. That is not
        # .........: yet implemented.

        # Idea: Take a hard problem and break it into 10,000
        # ....: simple problems. Solve all the simple problems.
        # ....: Combine all the results. And now you have the
        # ....: answer. I use algebra and some of of what I
        # ....: learned in calculus class. This is a slight
        # ....: variation on the trapezoid rule used in calculus.
        # ....: But instead of finding the area under the curve
        # ....: I am trying to find the length of a curve.
 
        # Use the Pythagorean Theorem to calculate pi.
        #
        # Calculate pi:
        # Steps:
        #   Step 1: Find a number of x and y points along
        #   ......: the path of a circle.
        #   Step 2: Find the distance between two adjacent
        #   ......: x,y points.
        #   Step 3: Sum of all the distances from 0 to 90
        #   ......: degrees.
        #   Step 4: Multiply the sum of all the distances
        #   ......: by 2.0 in order to get the length of
        #   ......: 180.0 degrees of a circle with a
        #   ......: radius of 1.0 inches/meters.

        version2 = version - 2
        circle_radius = 1.0
        circle_radius_squared = circle_radius ** 2.0
        line_length = 0.0
        
        # the value of the first point is...
        x_previous = 0.0
        y_previous = 1.0
                
        # Ideas:
        # Idea 1: for x = 0.0 to 0.5 
        # ......: math.sin(math.radians(30.0)) == 0.5
        # ......: and 180.0 / 30.0 == 6.0
        # Idea 2: for x = 0.0 to math.sqrt(2.0) 
        # ......: math.sin(math.radians(45.0)) == math.sqrt(2.0)
        # ......: and 180.0 / 45.0 == 4.0
        # Idea 3: for x = 0.0 to 1.0
        # ......: math.sin(math.radians(90.0)) == 1.0
        # ......: and 180.0 / 90.0 == 2.0

        # for x = 0.0 to 1.0 with a step value of
        # .. 1 / number_of_points
        for x_index in range(0, number_of_segments + 1):
            x = x_index / number_of_segments
            # Make sure the last value is exactly 1.0
            # This step is most likely unnecessary
            if x_index == number_of_segments:
                x = 1.0
            
            # Pythagorean Theorem
            # circle_radius ** 2.0 = x ** 2.0 + y ** 2.0
            # y ** 2.0 = circle_radius ** 2.0 - x ** 2.0
            y = math.sqrt(circle_radius_squared - x ** 2.0)
            
            # Find the distance of each segment
            distance = (
                find_the_distance_between_two_points(
                x_previous, y_previous, x, y,
                version = version2))
            
            line_length = line_length + distance
            
            x_previous = x
            y_previous = y
            
        # The line_length of 90 degrees * 2.0 should give us
        # the line_length of 180 degrees or pi
        pi = line_length * 2.0
        
    else:
        print(
"Error in function pi()\n"
"Version error")
        # Create an error just for fun.
        pi = 1.0 / 0.0
        pass # no operation (nop)

    return(pi)

def print_help():
    """
Print help.
"""
    print(
'For help.......: ./pi.py -h\n'
'For help2......: ./pi.py --help\n'
'For code test..: ./pi.py --test\n'
'For calculate pi: ./pi.py\n'
'\n'
'# import method:\n'
'import pi\n'
'print(pi.pi())\n'
'print(pi.pi(number_of_segments=100, version=2))\n'
'print(pi.pi(number_of_segments=1000, version=2))\n'
)

def print_help2():
    """
Print more help.
"""
    print_help()
    print(
'History:\n'
'I just wanted to see if I could figure out a way to\n'
'calculate the value of pi on my own back around 1987 on a\n'
'TANDY Pocket Computer PC-3 in BASIC with 2 kB of RAM.\n'
'Note: The TANDY Pocket Computer PC-3 uses BCD math.\n'
'I wanted to find a way to calculate pi using a method\n'
'that I could understand exactly how it worked.\n'
'\n'
'In celebration of Pi Day 2024 I decided to rewrite the\n'
'code in python3.\n'
'\n'
'Please note that there is more than one way to calculate\n'
'pi.\n'
)

def code_test():
    """
Code Test:
"""
    print("Version 2:", pi(version = 2))
    print("Version 1:", pi(version = 1))
    print("Version 0:", pi(version = 0))

# ASCII Ruler for 79 Characters for Code
#---|----1----|----2----|----3----|----4----|----5----|----6----|----7----|----
def main():
# ASCII Ruler for 72 Characters for Docstrings/Comments
#---|----1----|----2----|----3----|----4----|----5----|----6----|----7--
    """
Select the correct program to run.
"""
# ASCII Ruler for 79 Characters for Code
#---|----1----|----2----|----3----|----4----|----5----|----6----|----7----|----
    options_dictionary = {
        '-h':             print_help,
        '--help':         print_help2,
        '--test':         code_test,
    }

    # Run the correct function based on the options used
    if len(sys.argv) >= 2:
        options_dictionary.setdefault(sys.argv[1], print_help)()
    else:
        print("The approximate value of pi is:", pi())

if __name__ == '__main__':
    main()
